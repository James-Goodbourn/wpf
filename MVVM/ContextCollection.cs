﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Threading;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MVVM
{
    /// <summary>
    /// Provides a thread safe implementation of <see cref="INotifyCollectionChanged"/> without explicitly requiring a dispatcher.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ContextCollection<T> : IList<T>, INotifyCollectionChanged
    {
        private IList<T> m_Collection = new List<T>();
        public SynchronizationContext Context { get; private set; }
        private ReaderWriterLock sync = new ReaderWriterLock();

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Creates an instance of <see cref="ContextCollection"/> with a specific <see cref="IContext"/>.
        /// </summary>
        /// <remarks>Useful when <see cref="SafeObservable"/> access is required on a worker thread.</remarks>
        /// <param name="preferredDispatcher"></param>
        public ContextCollection(SynchronizationContext threadContext)
        {
            Context = threadContext;
        }
        public ContextCollection(SynchronizationContext threadContext, IEnumerable<T> items)
        {
            Context = threadContext;

            this.AddRange(items);
        }
        public virtual void Add(T item)
        {
            if (item == null)
                return;
            if (!Context.IsWaitNotificationRequired())
                    DoAdd(item);
            else Context.Send(new SendOrPostCallback(p => { DoAdd((T)p); }), item);
        }

        delegate void OperationDelegate(T item);
        delegate void OperationVoidDelegate();
        object lockOperation = new object();
        private void LockOperation(OperationDelegate del, T item)
        {
            lock (lockOperation)
            {
                del.Invoke(item);
            }
        }
        private void LockOperation(OperationVoidDelegate del)
        {
            lock (lockOperation)
            {
                del.Invoke();
            }
        }
        private void  DoAdd(T item)
        {
            sync.AcquireWriterLock(Timeout.Infinite);
            Monitor.Enter(lockOperation);
            m_Collection.Add(item);
            if (CollectionChanged != null)
                CollectionChanged(this,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
            Monitor.Exit(lockOperation);
            sync.ReleaseWriterLock();
        }
        private void DoAddRange(IEnumerable<T> items)
        {
            sync.AcquireWriterLock(Timeout.Infinite);
            Monitor.Enter(lockOperation);

            var list = items.ToList();  //deferred enumerations (eg. linq) break the change notification. Enumerate immediately.
            foreach (var i in list)
            {
                m_Collection.Add(i);
            }

            if (CollectionChanged != null)
                CollectionChanged(this,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new List<T>(list)));

            Monitor.Exit(lockOperation);
            sync.ReleaseWriterLock();
        }
        public virtual void AddRange(IEnumerable<T> items)
        {
            if (items == null)
                return;
            if (!Context.IsWaitNotificationRequired())
                DoAddRange(items);
            else
                Context.Send((SendOrPostCallback)(p => DoAddRange(items)), null);
        }

        public virtual void Clear()
        {
            if (!Context.IsWaitNotificationRequired())
                DoClear();
            else
                Context.Send((SendOrPostCallback)(p => { DoClear(); }), null);
        }

        private void DoClear()
        {
            sync.AcquireWriterLock(Timeout.Infinite);
            Monitor.Enter(lockOperation);
            m_Collection.Clear();
            if (CollectionChanged != null)
                CollectionChanged(this,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            Monitor.Exit(lockOperation);
            sync.ReleaseWriterLock();
        }

        public virtual bool Contains(T item)
        {
            sync.AcquireReaderLock(Timeout.Infinite);
            Monitor.Enter(lockOperation);
            var result = m_Collection.Contains(item);
            Monitor.Exit(lockOperation);
            sync.ReleaseReaderLock();
            return result;
        }

        public virtual void CopyTo(T[] array, int arrayIndex)
        {
            sync.AcquireWriterLock(Timeout.Infinite);
            Monitor.Enter(lockOperation);
            m_Collection.CopyTo(array, arrayIndex);
            Monitor.Exit(lockOperation);
            sync.ReleaseWriterLock();
        }

        public virtual int Count
        {
            get
            {
                sync.AcquireReaderLock(Timeout.Infinite);
                Monitor.Enter(lockOperation);
                var result = m_Collection.Count;
                Monitor.Exit(lockOperation);
                sync.ReleaseReaderLock();
                return result;
            }
        }

        public bool IsReadOnly
        {
            get { return m_Collection.IsReadOnly; }
        }

        public virtual bool Remove(T item)
        {
            bool result = false;
            if (!Context.IsWaitNotificationRequired())
                return DoRemove(item);
            else
            {
                Context.Send((SendOrPostCallback)(p => result = DoRemove((T)p)), item);
            }

            return result;
        }

        private bool DoRemove(T item)
        {
            sync.AcquireWriterLock(Timeout.Infinite);
            Monitor.Enter(lockOperation);
            var index = m_Collection.IndexOf(item);
            if (index == -1)
            {
                Monitor.Exit(lockOperation);
                sync.ReleaseWriterLock();
                return false;
            }
            var result = m_Collection.Remove(item);
            if (result && CollectionChanged != null)
                CollectionChanged(this, new
                    NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            Monitor.Exit(lockOperation);
            sync.ReleaseWriterLock();
            return result;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return m_Collection.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return m_Collection.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            sync.AcquireReaderLock(Timeout.Infinite);
            Monitor.Enter(lockOperation);
            var result = m_Collection.IndexOf(item);
            Monitor.Exit(lockOperation);
            sync.ReleaseReaderLock();
            return result;
        }

        public virtual void Insert(int index, T item)
        {
            if (!Context.IsWaitNotificationRequired())
                DoInsert(index, item);
            else
                Context.Send((SendOrPostCallback)(p => DoInsert(index, (T)p)), item);
        }

        private void DoInsert(int index, T item)
        {
            sync.AcquireWriterLock(Timeout.Infinite);
            Monitor.Enter(lockOperation);
            m_Collection.Insert(index, item);
            if (CollectionChanged != null)
                CollectionChanged(this,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
            Monitor.Exit(lockOperation);
            sync.ReleaseWriterLock();
        }

        public virtual void RemoveAt(int index)
        {
            if (!Context.IsWaitNotificationRequired())
                DoRemoveAt(index);
            else
                Context.Send((SendOrPostCallback)(p => DoRemoveAt(index)), null);
        }

        private void DoRemoveAt(int index)
        {
            sync.AcquireWriterLock(Timeout.Infinite);
            Monitor.Enter(lockOperation);
            if (m_Collection.Count == 0 || m_Collection.Count <= index)
            {
                Monitor.Exit(lockOperation);
                sync.ReleaseWriterLock();
                return;
            }
            m_Collection.RemoveAt(index);
            if (CollectionChanged != null)
                CollectionChanged(this,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            Monitor.Exit(lockOperation);
            sync.ReleaseWriterLock();

        }

        public T this[int index]
        {
            get
            {
                sync.AcquireReaderLock(Timeout.Infinite);
                Monitor.Enter(lockOperation);
                var result = m_Collection[index];
                Monitor.Exit(lockOperation);
                sync.ReleaseReaderLock();
                return result;
            }
            set
            {
                sync.AcquireWriterLock(Timeout.Infinite);
                Monitor.Enter(lockOperation);
                if (m_Collection.Count == 0 || m_Collection.Count <= index)
                {
                    Monitor.Exit(lockOperation);
                    sync.ReleaseWriterLock();
                    return;
                }
                m_Collection[index] = value;
                Monitor.Exit(lockOperation);
                sync.ReleaseWriterLock();
            }

        }
    }
}
