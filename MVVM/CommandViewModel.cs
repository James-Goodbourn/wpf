﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MVVM
{
    /// <summary>
    /// Provides a view model representation of an <see cref="ICommand"/>
    /// </summary>
    public class CommandViewModel: ViewModel
    {
        /// <summary>
        /// Command associated with this view model.
        /// </summary>
        public ICommand Command
        {
            get { return m_Command; }
            set
            {
                m_Command = value;
                base.OnPropertyChanged(() => Command);
            }
        } ICommand m_Command = default(ICommand);
        /// <summary>
        /// Gets or sets a verbose description for this command.
        /// </summary>
        public string Description
        {
            get { return m_Description; }
            set
            {
                m_Description = value;
                base.OnPropertyChanged(() => Description);
            }
        } string m_Description = default(string);

    }
}
