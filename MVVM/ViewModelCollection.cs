﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVVM
{
    /// <summary>
    /// Implementation of <see cref="ContextCollection"/> that will automatically remove view models that request closure.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ViewModelCollection<T> : ContextCollection<T> where T : ViewModel
    {
        public ViewModelCollection(System.Threading.SynchronizationContext context):base(context)
        {
        }

        public override void Add(T item)
        {
            OnAdd(item);

            base.Add(item);
        }

        public override void AddRange(IEnumerable<T> items)
        {
            var arr = items.ToArray();  //make sure deferred execution stuff 

            foreach (var vm in arr)
            {

                OnAdd(vm);
            }
            base.AddRange(arr);
        }

        public override bool Remove(T item)
        {
            OnRemove(item);
            return base.Remove(item);
        }

        public override void Clear()
        {
            foreach (var vm in this)
            {
                OnRemove(vm);
            }
            base.Clear();
        }

        public override void RemoveAt(int index)
        {
            var vm = this[index]; //may cause exception, dont bother checking
            if (vm != null)
                this.OnRemove(vm);

            base.RemoveAt(index);
        }

        void item_RequestingClose(object sender, EventArgs e)
        {
            this.Remove((T)sender);
        }

        protected virtual void OnAdd(T item)
        {
            if (!this.Contains(item))
                item.Closed += new EventHandler(item_RequestingClose);
            else
            {
            }
        }

        protected virtual void OnRemove(T item)
        {
            item.Closed -= new EventHandler(item_RequestingClose);
        }
    }
}
