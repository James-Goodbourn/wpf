﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;

namespace MVVM
{
    public class RelayKeyBinding : KeyBinding
    {
        #region CommandBindingProperty
        public ICommand CommandBinding
        {
            get { return (ICommand)GetValue(CommandBindingProperty); }
            set { SetValue(CommandBindingProperty, value); }
        }

        public static readonly DependencyProperty CommandBindingProperty =
            DependencyProperty.Register("CommandBinding", typeof(ICommand),
            typeof(RelayKeyBinding),
            new FrameworkPropertyMetadata(OnCommandBindingChanged));

        private static void OnCommandBindingChanged(
            DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //System.Windows.Controls.Button b = new System.Windows.Controls.Button();
            var keyBinding = (RelayKeyBinding)d;

            if(e.NewValue != null)
                keyBinding.Command = (ICommand)e.NewValue;
        } 
        #endregion

        static RelayKeyBinding()
        {
            KeyBinding.CommandParameterProperty.OverrideMetadata(typeof(RelayKeyBinding), new PropertyMetadata(OnCommandParameterChanged));
        }

        #region CommandParameterProperty

        static void OnCommandParameterChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as KeyBinding).CommandParameter = e.NewValue;
            
        } 
        #endregion
    }
}
