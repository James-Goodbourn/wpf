﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Diagnostics;

namespace MVVM
{
    public class RelayCommandManager
    {
        const int tickLength = 200;

        static object padLock = new object();
        static List<WeakReference> commands = new List<WeakReference>();
        static System.Threading.Timer timer = null;
        static RelayCommandManager()
        {
            CreateTimer();
        }
        ~RelayCommandManager()
        {
            timer.Dispose();
        }
        public static void Register(RelayCommand command)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            lock (padLock)
            {
                commands.Add(new WeakReference(command));
            }

            StartTimer();
        }

        static void CreateTimer()
        {
            if (timer == null)
                timer = new System.Threading.Timer(TimerTick);
        }
        static void TimerTick(object state)
        {
            StopTimer();

            lock (padLock)
            {
                commands.RemoveAll(p => !p.IsAlive);    //remove all dead commands
            }

            if (commands.Count > 0) //requery all commands if commands atill alive
            {
                CommandManager.InvalidateRequerySuggested();
                StartTimer();
            }
        }
        static void StopTimer()
        {
            timer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
        }
        static void StartTimer()
        {
            if (timer == null)
                CreateTimer();

            timer.Change(tickLength, System.Threading.Timeout.Infinite);
        }
    }
    /// <summary>
    /// A command whose sole purpose is to 
    /// relay its functionality to other
    /// objects by invoking delegates. The
    /// default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : System.Windows.Input.ICommand
    {
        #region Fields
        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;

            RelayCommandManager.Register(this);
        }

        #endregion // Constructors

        #region ICommand Members

        //[DebuggerStepThrough]
        public virtual bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public virtual void Execute(object parameter)
        {
            if(_execute != null)
                _execute(parameter);
        }

        #endregion // ICommand Members
    }
}
