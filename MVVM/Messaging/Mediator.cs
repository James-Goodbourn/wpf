﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace MVVM.Messaging
{
    public class Mediator
    {
        /// <summary>
        /// Represents an exception that occured in the <see cref="Mediator"/>.
        /// </summary>
        public class MediatorException : System.Exception
        {
            public MediatorException(string message)
                : base(message)
            {
            }
            public MediatorException(string message, Exception innerException)
                : base(message, innerException)
            {
            }
        }

        class WeakAction
        {
            WeakReference m_Reference = null;
            MethodInfo m_Method = null;
            Type m_DelegateType = null;
            public Type ParameterType { get; private set; }
            public bool IsAlive
            {
                get
                {
                    if (m_Reference == null)
                        return false;

                    return m_Reference.IsAlive;
                }
            }
            public bool IsStatic { get; set; }
            public object Target
            {
                get
                {
                    return m_Reference.Target;
                }
            }

            public WeakAction(object target, MethodInfo methodInfo, Type parameterType)
            {
                if (methodInfo == null)
                    throw new ArgumentNullException("MethodInfo cannot be null");

                IsStatic = target == null;
                m_Reference = new WeakReference(target);

                m_Method = methodInfo;
                ParameterType = parameterType;

                //Assign what type of delegate this should be (if parameters are required)
                if (parameterType == null)
                    this.m_DelegateType = typeof(Action);
                else
                    this.m_DelegateType = typeof(Action<>).MakeGenericType(parameterType);
            }
            /// <summary>
            /// Gets a <see cref="Delegate"/> to the target action.
            /// </summary>
            /// <returns></returns>
            public Delegate GetDelegate()
            {
                var newDelegate = Delegate.CreateDelegate(
                    m_DelegateType,
                    m_Reference.Target,
                    m_Method);

                return newDelegate;
            }
        }

        static object actionsLock = new object();
        static System.Collections.Concurrent.ConcurrentDictionary<object, List<WeakAction>> actions = new System.Collections.Concurrent.ConcurrentDictionary<object, List<WeakAction>>();

        public static bool ThrowOnArgumentMismatch { get; set; }

        static Mediator()
        {
            ThrowOnArgumentMismatch = false;
        }

        /// <summary>
        /// Registers an object for mesages. All public methods on the target decorated with <see cref="MediatorMessageSink"/> will recieve appropriate messages.
        /// </summary>
        /// <param name="target">Object to register.</param>
        /// <returns>False is unsuccessful.</returns>
        /// <exception cref="NotSupportedException">Thrown if the signature of a decorated method contains more than one parameter.</exception>
        internal static void Register(object target)
        {
            //Get public, instance Methods
            MethodInfo[] methods = target.GetType().GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);

            //Loop all methods on the targets
            foreach (MethodInfo mInfo in methods)
            {
                var atts = mInfo.GetCustomAttributes(typeof(MediatorMessageSink), true);

                //Message sink attribute(s) found, register
                if (atts.Length > 0)
                {
                    Type paramType = null;
                    var parameters = mInfo.GetParameters();

                    if (parameters.Length > 1)
                        throw new NotSupportedException(string.Format("No more than 1 parameter is supported on method {0}", mInfo.Name));

                    if (parameters.Length == 1)
                        paramType = parameters[0].ParameterType;               

                    foreach (MediatorMessageSink att in atts.OfType<MediatorMessageSink>())
                    {
                        //Register method for messages.
                        AddAction(att.MessageKey, target, mInfo, paramType);
                    }
                }
            }
        }

        /// <summary>
        /// Register an <see cref="Action&lt;T&gt;"/> delegate.
        /// </summary>
        /// <typeparam name="T">Parameter type.</typeparam>
        /// <param name="messageKey">Message key to subscribe to.</param>
        /// <param name="action">Message sink delegate.</param>
        public static void Register<T>(object messageKey, Action<T> action)
        {
            Register(messageKey, (Delegate)action);
        }
        /// <summary>
        /// Register an <see cref="Action"/> delegate.
        /// </summary>
        /// <param name="messageKey">Message key to subscribe to.</param>
        /// <param name="action">Message sink delegate.</param>
        public static void Register(object messageKey, Action action)
        {
            Register(messageKey, (Delegate)action);
        }
        private static void Register(object messageKey, Delegate action)
        {
            //This method is not supported in Silverlight
#if SILVERLIGHT
            throw new InvalidOperationException("This method is not supported in Silverlight");
#endif

            //if (action.Target == null)
            //   throw new InvalidOperationException("Delegate cannot be static");

            var pInfo = action.Method.GetParameters();

            if (pInfo.Length > 1)
                throw new NotSupportedException("No more than 1 parameter is supported");

            Type actionType = (pInfo == null || pInfo.Length == 0) ? null : pInfo[0].ParameterType;

            if (action.Target == null)
                AddAction(messageKey, null, action.Method, actionType);
            else 
                AddAction(messageKey, action.Target, action.Method, actionType);
        }
        /// <summary>
        /// Gets all the actions relating to a given message key.
        /// </summary>
        /// <param name="messageKey">Message key for which to return actions.</param>
        /// <returns></returns>
        private static WeakAction[] GetActions(object messageKey, Type parameterType = null)
        {
            lock (actionsLock)
            {
                if (actions.ContainsKey(messageKey))
                {
                    List<WeakAction> retVals = new List<WeakAction>();
                    var matches = actions[messageKey].ToArray();

                    for (int i = matches.Length - 1; i >= 0; i--)
                    {
                        //Check if target exists, if it has been garbage collected, remove the action.
                        if (!matches[i].IsAlive && !matches[i].IsStatic)
                        {
                            actions[messageKey].Remove(matches[i]);
                            continue;
                        }

                        if (matches[i].ParameterType == null && parameterType == null)
                            retVals.Add(matches[i]);
                        else if (matches[i].ParameterType.IsAssignableFrom(parameterType)) //will allow any derived class, however, not implicit operators
                            retVals.Add(matches[i]);
                    }

                    return retVals.ToArray();
                }
                else return null;
            }
        }
        private static void AddAction(object messageKey, object target, MethodInfo methodInfo, Type actionType = null)
        {
            lock (actionsLock)
            {
                if (actions.ContainsKey(messageKey))
                {
                    //Do not duplicate registered targets for a given key
                    foreach (WeakAction action in actions[messageKey])
                    {
                        if (action.GetDelegate().Method == methodInfo)
                        {
                            if (action.IsStatic)
                                return;
                            else if (action.IsAlive && action.Target == target)
                                return;
                        }
                    }
                }

                var act = new WeakAction(target, methodInfo, actionType);

                actions.AddOrUpdate(messageKey, new List<WeakAction>{ act }, (k, c) => { c.Add(act); return c; });
            }
        }
        /// <summary>
        /// Posts a message to the <see cref="Mediator"/> to notify subscribers
        /// </summary>
        /// <param name="messageKey">Message key to notify subscribers of.</param>
        public static void PostMessage(object messageKey)
        {
            var actions = GetActions(messageKey);

            if (actions != null)
            {
                foreach (WeakAction act in actions)
                {
                    try
                    {
                        act.GetDelegate().DynamicInvoke();
                    }
                    catch (ArgumentException ex)
                    {
                        //Continue but ignore this invokation
                        if (ThrowOnArgumentMismatch)
                            throw new MediatorException(ex.Message, ex);
                    }
                }
            }
        }
        /// <summary>
        /// Posts a message to the <see cref="Mediator"/> to notify subscribers
        /// </summary>
        /// <param name="messageKey">Message key to notify subscribers of.</param>
        /// <param name="argument">Argument to post to subscribers.</param>
        public static void PostMessage<T>(object messageKey, T argument)
        {
            var actions = GetActions(messageKey, typeof(T));

            if (actions != null)
            {
                foreach (WeakAction act in actions)
                {
                    try
                    {
                        act.GetDelegate().DynamicInvoke(argument);
                    }
                    catch (ArgumentException ex)
                    {
                        //Continue but ignore this invokation
                        if (ThrowOnArgumentMismatch)
                            throw new MediatorException(ex.Message, ex);
                    }                 
                }
            }
        }


    }
}
