﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVVM.Messaging
{
    public class NavigationArgs<T>
    {
        public T Destination { get; set; }
    }
    /// <summary>
    /// Service for managing navigation through items. Stores history.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NavigationService<T> where T : INavigable
    {
        class NavigationNode<O>
        {
            /// <summary>
            /// Gets or sets if this node's content can be navigated back to.
            /// </summary>
            public bool Preserve { get; set; }

            public O Content { get; set; }

            public NavigationNode()
            {
                Preserve = true;
            }

            public NavigationNode(O content):this()
            {
                Content = content;
            }
        }

        object padlock = new object();
        Stack<NavigationNode<T>> m_NavStack = new Stack<NavigationNode<T>>();

        private object m_MessageKey = null;

        public NavigationService(object messageKey)
        {
            m_MessageKey = messageKey;
        }
        /// <summary>
        /// Return trues if pages exists to navigate back to.
        /// </summary>
        public bool HasHistory
        {
            get { return ItemCount > 1; }
        }
        /// <summary>
        /// Gets the number of items that exist in the back stack.
        /// </summary>
        public int ItemCount 
        {
            get
            {
                return m_NavStack.Count;
            }
        }
        /// <summary>
        /// The last item in the back stack that was navigated to.
        /// </summary>
        NavigationNode<T> CurrentNode { get { return m_NavStack.Count > 0 ? m_NavStack.Peek() : null; } }
        public T CurrentItem { get { return CurrentNode == null ? default(T) : CurrentNode.Content; } }

        public void NavigateTo(T destination, bool record = true)
        {
            if (destination == null) throw new ArgumentNullException("Destination must not be null");

            lock (padlock)
            {
                if (!destination.PreNavigate()) return;

                NavigationNode<T> newNode = new NavigationNode<T>(destination);
                newNode.Preserve = record;
                 
                //tidy up item that is being navigated away from
                if (CurrentNode != null)
                {
                    if(CurrentNode.Content != null)
                        CurrentNode.Content.OnNavigateAway();

                    //if not preserving, remove from graph.
                    if (!CurrentNode.Preserve)
                    {
                        m_NavStack.Pop();
                    }
                }

                Mediator.PostMessage(m_MessageKey, new NavigationArgs<T>() 
                {
                    Destination = destination
                });

                m_NavStack.Push(newNode);

                if(destination != null)
                    destination.OnNavigateTo();
            }
        }

        /// <summary>
        /// Clears history, preserving the first item and the current item.
        /// </summary>
        public void ClearHistory()
        {
            lock (padlock)
            {
                var currentNode = CurrentNode;

                while (m_NavStack.Count > 1)
                    m_NavStack.Pop();

                if (currentNode != null)
                    m_NavStack.Push(currentNode);
            }
        }

        public void NavigateBack()
        {
            lock (padlock)
            {
                if (HasHistory)
                {
                    NavigationNode<T> newNode = null;
                    var oldNode = m_NavStack.Pop();

                    while(true)
                    {
                        if (m_NavStack.Count < 1) return;

                        newNode = m_NavStack.Peek();

                        if (newNode.Content.PreNavigate())
                        {
                            if (oldNode != null && oldNode.Content != null)
                                oldNode.Content.OnNavigateAway();

                            Mediator.PostMessage(m_MessageKey, new NavigationArgs<T>() { Destination = newNode.Content });

                            if (newNode != null && newNode.Content != null)
                            {
                                newNode.Content.OnNavigateTo();
                            }

                            break;
                        }
                        else m_NavStack.Pop(); //remove page if it denies navigation
                    }
                }
            }
        }
    }
}
