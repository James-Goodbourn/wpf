﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVVM.Messaging
{
    /// <summary>
    /// Marks up a method as a destination for mediator messages.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class MediatorMessageSink : System.Attribute
    {
        /// <summary>
        /// Message to send.
        /// </summary>
        public object MessageKey { get; set; }
        /// <summary>
        /// The type of the message parameter.
        /// </summary>
        public Type ParameterType { get; set; }
        /// <param name="messageKey"
        /// >Message to send.
        /// </param>
        public MediatorMessageSink(object messageKey)
        {
            MessageKey = messageKey;
        }
    }
}