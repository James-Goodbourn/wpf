﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVVM.Messaging
{
    public interface INavigable
    {
        /// <summary>
        /// Occurs just before the page is navigated to. Implementations should return false to skip this page.
        /// </summary>
        bool PreNavigate();
        /// <summary>
        /// Occurs when the item is navigated to.
        /// </summary>
        void OnNavigateTo();
        /// <summary>
        /// Occurs when another item is navigated to from this item.
        /// </summary>
        void OnNavigateAway();
    }
}
