﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;

namespace MVVM
{
    /// <summary>
    /// Simple view model class featuring a display name and <see cref="System.ComponentModel.INotifyPropertyChanged"/> implementation.
    /// </summary>
    public abstract class ViewModelBase : System.ComponentModel.INotifyPropertyChanged, IDisposable
    {
        bool m_IsDisposed = false;
        string _DisplayName = string.Empty;
        object m_Tag = null;

        public object Tag
        {
            get { return m_Tag; }
            set 
            { 
                m_Tag = value;
                OnPropertyChanged("Tag");
            }
        }
        
        /// <summary>
        /// Returns the user-friendly name of this object.
        /// Child classes can set this property to a new value,
        /// or override it to determine the value on-demand.
        /// </summary>
        public virtual string DisplayName 
        {
            get { return _DisplayName; }
            set
            {
                _DisplayName = value;
                this.OnPropertyChanged("DisplayName");
            }
        }

        public ViewModelBase()
        {
            this.DisplayName = this.GetType().ToString();
        }

        /// <summary>
        /// Useful for ensuring that ViewModel objects are properly garbage collected.
        /// </summary>
        ~ViewModelBase()
        {
            if (!m_IsDisposed)
                this.Dispose();

#if DEBUG
            string msg = string.Format("{0} ({1}) ({2}) Finalized", this.GetType().Name, this.DisplayName, this.GetHashCode());
            System.Diagnostics.Debug.WriteLine(msg);
#endif
        }

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);

            if (this.PropertyChanged != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                this.PropertyChanged(this, e);
            }
        }

        //public sealed class Lambda<T>
        //{
        //    public static Func<T, T> Cast;
        //}

        protected virtual void OnPropertyChanged<TProperty>(Expression<Func<TProperty>> property)
        {
            this.OnPropertyChanged(this.GetMemberNameFromExpression(property));
        }

        public string GetMemberNameFromExpression<TProperty>(Expression<Func<TProperty>> property)
        {
            var lambda = property as LambdaExpression;

            if (lambda != null)
            {
                MemberExpression memberExpression = null;
                if (lambda.Body is UnaryExpression)
                {
                    var unary = lambda.Body as UnaryExpression;
                    memberExpression = (MemberExpression)unary.Operand;
                }
                else
                {
                    memberExpression = (MemberExpression)lambda.Body;
                }

                System.Diagnostics.Debug.Assert(memberExpression != null, "Expression is not in the correct format ( () => SomeProperty )");

                if (memberExpression != null)
                {
                    return memberExpression.Member.Name;
                }
            }

            return string.Empty;
        }

        #endregion // INotifyPropertyChanged Members

        #region IDisposable Members

        /// <summary>
        /// Invoked when this object is being removed from the application
        /// and will be subject to garbage collection.
        /// </summary>
        public virtual void Dispose()
        {
            m_IsDisposed = true;
            this.OnDispose();
        }

        /// <summary>
        /// Child classes can override this method to perform 
        /// clean-up logic, such as removing event handlers.
        /// </summary>
        protected virtual void OnDispose()
        {
        }

        #endregion // IDisposable Members

        #region Debugging Aides

        /// <summary>
        /// Warns the developer if this object does not have
        /// a public property with the specified name. This 
        /// method does not exist in a Release build.
        /// </summary>
        [Conditional("DEBUG")]
        //[DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,  
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                    throw new Exception(msg);
                else
                    Debug.Fail(msg);
            }
        }

        /// <summary>
        /// Returns whether an exception is thrown, or if a Debug.Fail() is used
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// The default value is false, but subclasses used by unit tests might 
        /// override this property's getter to return true.
        /// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }

        #endregion // Debugging Aides

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
