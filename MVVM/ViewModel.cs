﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace MVVM
{
    /// <summary>
    /// Suggested base class for ViewModel classes in the application.
    /// It provides support for property change notifications 
    /// and has a DisplayName property.  This class is abstract.
    /// </summary>
    public abstract class ViewModel : ViewModelBase, INotifyPropertyChanged, IDataErrorInfo
    {
        #region Constructor

        protected ViewModel()
        {
            CloseCommand = new RelayCommand(p => Close());
            Errors = new Dictionary<string, List<string>>();

            Messaging.Mediator.Register(this);
        }

        #endregion // Constructor

        #region Properties

        public RelayCommand CloseCommand
        {
            get { return m_CloseCommand; }
            set
            {
                m_CloseCommand = value;
                base.OnPropertyChanged(() => CloseCommand);
            }
        } RelayCommand m_CloseCommand = default(RelayCommand);
        public bool ValidationOK { get { return Errors.Count == 0; } }
        public Dictionary<string, List<string>> Errors { get; protected set; }
        #endregion

        /// <summary>
        /// Raised when this workspace should be removed from the UI.
        /// </summary>
        public event EventHandler Closed;

        public virtual void Close()
        {
            EventHandler handler = this.Closed;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        #region IDataErrorInfo Members
        string IDataErrorInfo.Error
        {
            get { return null; }
        }

        string IDataErrorInfo.this[string columnName]
        {
            get { return GetCurrentError(columnName); }
        }

        public void AddError(string propertyName, string error, bool isWarning = false)
        {
            if (!Errors.ContainsKey(propertyName))
                Errors[propertyName] = new List<string>();

            if (!Errors[propertyName].Contains(error))
            {
                if (isWarning) Errors[propertyName].Add(error);
                else Errors[propertyName].Insert(0, error);
            }

            OnPropertyChanged("Errors");
            OnPropertyChanged("ValidationOK");
        }
        public void RemoveError(string propertyName, string error)
        {
            if (Errors.ContainsKey(propertyName) &&
                Errors[propertyName].Contains(error))
            {
                Errors[propertyName].Remove(error);
                if (Errors[propertyName].Count == 0)
                {
                    Errors.Remove(propertyName);
                    OnPropertyChanged("Errors");
                    OnPropertyChanged("ValidationOK");
                }
            }
        }
        public void RemovePropertyErrors(string columnName)
        {
            Errors.Remove(columnName);
            OnPropertyChanged("Errors");
            OnPropertyChanged("ValidationOK");
        }
        string GetCurrentError(string columnName)
        {
            return (!Errors.ContainsKey(columnName) ? null :
                   String.Join(Environment.NewLine, Errors[columnName].ToArray()));
        }
        #endregion
    }
}
